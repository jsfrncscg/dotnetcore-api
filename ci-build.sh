
###################################################################################
# Cleanup for Jenkins \
###################################################################################
rm -rf ./bin/*
rm -rf ./obj/*
rm -rf ./*.tar
# Will fail when no prior images are found
docker rmi $(docker images | grep 'dotnetcore-api' | tr -s ' ' | cut -d ' ' -f 3)
docker volume ls -qf dangling=true | xargs -r docker volume rm
###################################################################################

PROJECT_FOLDER="./dotnetcore-api"
VERSION=$(date +"%y%m.%d.%H.%M")
DOCKER_PUBLISH_FOLDER="./obj/Docker/publish"
DOCKER_IMAGE_ID=vkhazin/dotnetcore-api:$VERSION
###################################################################################
# Moved to env variables
####################################################################################
# S3_BUCKET_NAME="smith-poc-deploy"
# S3_BUCKET_KEY="dotnetcore-api"
###################################################################################
FILE_NAME="${DOCKER_IMAGE_ID/:/-}"
FILE_NAME="${FILE_NAME/\//-}".tar

# Build
dotnet restore $PROJECT_FOLDER
dotnet publish $PROJECT_FOLDER -c Release -o $DOCKER_PUBLISH_FOLDER
echo 'Finished building the project'

# Package
docker build $PROJECT_FOLDER --tag $DOCKER_IMAGE_ID
docker save -o $FILE_NAME $DOCKER_IMAGE_ID
echo "Finished creating and extracting docker image: $DOCKER_IMAGE_ID"

# Upload to S3
aws s3api put-object --bucket $S3_BUCKET_NAME --key $S3_BUCKET_KEY/$FILE_NAME --body $FILE_NAME
echo "Finished uploading docker image tar file to S3 bucket: $S3_BUCKET_KEY\/$FILE_NAME"