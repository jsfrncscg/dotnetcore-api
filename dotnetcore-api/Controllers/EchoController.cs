﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace dotnetapi.Controllers
{
    [Route("")]
    [Route("[controller]")]
    public class EchoController : Controller
    {
        private const string _defaultName = "World";

        [HttpGet("")]
        [HttpGet("{name}")]
        public async Task<IActionResult> Get(string name = _defaultName)
        {
            return await Task.Run(() => {
                dynamic response = new System.Dynamic.ExpandoObject();
                response.message = $"Hello {name}!";
                response.timestamp = DateTime.Now.ToUniversalTime();
                response.host = Environment.MachineName;

                if (name.Equals(_defaultName, StringComparison.CurrentCultureIgnoreCase)) {
                    response.hint = "Add forwardslash and your name to the url, e.g.: hostname:port/my name";
                }

                return Ok(response);
            });
        }
    }
}
